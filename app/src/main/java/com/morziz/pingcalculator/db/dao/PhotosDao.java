package com.morziz.pingcalculator.db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.morziz.pingcalculator.data.db.Photo;
import com.morziz.pingcalculator.data.db.Post;

import java.util.List;

@Dao
public interface PhotosDao {

    @Insert
    void insert(Photo photo);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Photo> photos);

    @Delete
    void delete(Photo photo);

    @Update
    void update(Photo photo);

    @Query("DELETE FROM photos")
    public void deleteAll();

}
