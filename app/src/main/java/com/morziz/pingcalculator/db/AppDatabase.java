package com.morziz.pingcalculator.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.morziz.pingcalculator.data.db.Comment;
import com.morziz.pingcalculator.data.db.Photo;
import com.morziz.pingcalculator.data.db.PingLog;
import com.morziz.pingcalculator.data.db.Post;
import com.morziz.pingcalculator.data.db.ToDo;
import com.morziz.pingcalculator.db.dao.CommentsDao;
import com.morziz.pingcalculator.db.dao.PhotosDao;
import com.morziz.pingcalculator.db.dao.PingLogsDao;
import com.morziz.pingcalculator.db.dao.PostsDao;
import com.morziz.pingcalculator.db.dao.ToDosDao;

@Database(entities = {PingLog.class, Comment.class, Photo.class, Post.class, ToDo.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract PingLogsDao pingLogsDao();

    public abstract CommentsDao commentsDao();

    public abstract PhotosDao photosDao();

    public abstract PostsDao postsDao();

    public abstract ToDosDao toDosDao();
}