package com.morziz.pingcalculator.db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.morziz.pingcalculator.data.db.Photo;
import com.morziz.pingcalculator.data.db.Post;
import com.morziz.pingcalculator.data.db.ToDo;

import java.util.List;

@Dao
public interface ToDosDao {

    @Insert
    void insert(ToDo toDo);

    @Delete
    void delete(ToDo toDo);

    @Update
    void update(ToDo toDo);

    @Query("DELETE FROM to_dos")
    public void deleteAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<ToDo> toDos);

}
