package com.morziz.pingcalculator.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.morziz.pingcalculator.data.db.PingLog;

import java.util.List;

@Dao
public interface PingLogsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(PingLog pingLog);

    @Delete
    void delete(PingLog pingLog);

    @Update
    void update(PingLog pingLog);

    @Query("DELETE FROM ping_logs")
    public void deleteAll();

    @Query("Select * from ping_logs where request_type = :type")
    PingLog getPingLogById(int type);

    @Query("Select * from ping_logs")
    LiveData<List<PingLog>> observeAllData();
}
