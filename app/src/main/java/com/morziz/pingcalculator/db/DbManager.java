package com.morziz.pingcalculator.db;

import android.content.Context;

import androidx.room.Room;

public class DbManager {

    private static DbManager instance;
    private AppDatabase appDatabase;

    private DbManager() {
    }

    public static synchronized DbManager getInstance() {
        if (instance == null) {
            instance = new DbManager();
        }
        return instance;
    }

    public void init(Context context) {
        if (appDatabase == null) {
            buildDb(context);
        }
    }

    private void buildDb(Context context) {
        appDatabase = Room.databaseBuilder(
                context,
                AppDatabase.class,
                "app_database.db"
        ).fallbackToDestructiveMigration().build();
    }

    public AppDatabase getDb() {
        return appDatabase;
    }

    public void clearLogs() {
        appDatabase.pingLogsDao().deleteAll();
    }
}
