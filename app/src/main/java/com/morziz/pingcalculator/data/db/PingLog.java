package com.morziz.pingcalculator.data.db;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.gson.annotations.SerializedName;
import com.morziz.pingcalculator.db.DateTypeConverter;

import java.util.Date;

@Entity(tableName = "ping_logs")
public class PingLog {

//    @PrimaryKey(autoGenerate = true)
//    private Long id;

    public PingLog() {
    }

    public PingLog(int requestType) {
        this.requestType = requestType;
    }

    @SerializedName("url")
    private String url;

    @SerializedName("response")
    private String response;

    @NonNull
    @PrimaryKey
    @SerializedName("requestType")
    @ColumnInfo(name = "request_type")
    private int requestType;

    @TypeConverters({DateTypeConverter.class})
    @SerializedName("apiStartTimestamp")
    @ColumnInfo(name = "api_start_timestamp")
    private Date apiStartTimestamp;

    @TypeConverters({DateTypeConverter.class})
    @SerializedName("apiEndTimestamp")
    @ColumnInfo(name = "api_end_timestamp")
    private Date apiEndTimestamp;

    @TypeConverters({DateTypeConverter.class})
    @SerializedName("saveStartTimestamp")
    @ColumnInfo(name = "save_start_timestamp")
    private Date saveStartTimestamp;

    @TypeConverters({DateTypeConverter.class})
    @SerializedName("saveEndTimestamp")
    @ColumnInfo(name = "save_end_timestamp")
    private Date saveEndTimestamp;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Date getApiStartTimestamp() {
        return apiStartTimestamp;
    }

    public void setApiStartTimestamp(Date apiStartTimestamp) {
        this.apiStartTimestamp = apiStartTimestamp;
    }

    public Date getApiEndTimestamp() {
        return apiEndTimestamp;
    }

    public void setApiEndTimestamp(Date apiEndTimestamp) {
        this.apiEndTimestamp = apiEndTimestamp;
    }

    public Date getSaveStartTimestamp() {
        return saveStartTimestamp;
    }

    public void setSaveStartTimestamp(Date saveStartTimestamp) {
        this.saveStartTimestamp = saveStartTimestamp;
    }

    public Date getSaveEndTimestamp() {
        return saveEndTimestamp;
    }

    public void setSaveEndTimestamp(Date saveEndTimestamp) {
        this.saveEndTimestamp = saveEndTimestamp;
    }

    public int getRequestType() {
        return requestType;
    }

    public void setRequestType(int requestType) {
        this.requestType = requestType;
    }


    @Override
    public String toString() {
        return "RequestType=" + requestType +
                ", apiStartTimestamp=" + apiStartTimestamp +
                ", apiEndTimestamp=" + apiEndTimestamp +
                ", saveStartTimestamp=" + saveStartTimestamp +
                ", saveEndTimestamp=" + saveEndTimestamp +
                '}';
    }
}
