package com.morziz.pingcalculator.liveactions;

public enum LiveEvent {
    GET_COMMENTS,
    GET_PHOTOS,
    GET_TODOs,
    GET_POSTS
}
