package com.morziz.pingcalculator.network.apis;

import com.morziz.pingcalculator.data.db.Comment;
import com.morziz.pingcalculator.data.db.Photo;
import com.morziz.pingcalculator.data.db.Post;
import com.morziz.pingcalculator.data.db.ToDo;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface APIContract {

    @GET("comments")
    Observable<List<Comment>> getComments();

    @GET("photos")
    Observable<List<Photo>> getPhotos();

    @GET("todos")
    Observable<List<ToDo>> getToDos();

    @GET("posts")
    Observable<List<Post>> getPosts();

}
