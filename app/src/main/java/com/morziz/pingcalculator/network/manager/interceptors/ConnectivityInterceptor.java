package com.morziz.pingcalculator.network.manager.interceptors;

import android.content.Context;

import com.morziz.pingcalculator.R;
import com.morziz.pingcalculator.network.manager.NoConnectivityException;
import com.morziz.pingcalculator.network.utils.ConnectivityUtils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class ConnectivityInterceptor implements Interceptor {
 
    private Context mContext;
 
    public ConnectivityInterceptor(Context context) {
        mContext = context;
    }
 
    @Override
    public Response intercept(Chain chain) throws IOException {
        if (!ConnectivityUtils.isNetworkEnabled(mContext)) {
            throw new NoConnectivityException(mContext.getString(R.string.network_error));
        }
 
        Request.Builder builder = chain.request().newBuilder();
        return chain.proceed(builder.build());
    }
 
}