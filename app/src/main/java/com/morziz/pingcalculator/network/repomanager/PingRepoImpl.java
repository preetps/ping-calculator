package com.morziz.pingcalculator.network.repomanager;

import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.morziz.pingcalculator.Constants;
import com.morziz.pingcalculator.data.db.Comment;
import com.morziz.pingcalculator.data.db.Photo;
import com.morziz.pingcalculator.data.db.PingLog;
import com.morziz.pingcalculator.data.db.Post;
import com.morziz.pingcalculator.data.db.ToDo;
import com.morziz.pingcalculator.db.DbManager;
import com.morziz.pingcalculator.network.manager.AppNetworkManager;
import com.morziz.pingcalculator.network.rx.AppRxSchedulers;
import com.morziz.pingcalculator.network.rx.RetryAPI;

import java.util.Date;
import java.util.List;

import io.reactivex.Observable;

public class PingRepoImpl implements PingRepo {

    private static PingRepo pingRepo;

    private PingRepoImpl() {
    }

    public static PingRepo getInstance() {
        if (pingRepo == null) {
            pingRepo = new PingRepoImpl();
        }
        return pingRepo;
    }

    @Override
    public Observable<List<Comment>> fetchComments() {
        return AppNetworkManager.getAppClient().getComments()
                .compose(new AppRxSchedulers().applySchedulers()).retryWhen(RetryAPI.retryWithExponentialBackOff()).flatMap(list -> {
                    AsyncTask.execute(() -> {
                        updateApiEndTime(Constants.REQUEST_TYPE.COMMENTS);
                        updateDbSaveStartTime(Constants.REQUEST_TYPE.COMMENTS);
                        DbManager.getInstance().getDb().commentsDao().insert(list);
                        updateDbSaveEndTime(Constants.REQUEST_TYPE.COMMENTS);
                    });
                    return Observable.just(list);
                });
    }

    @Override
    public Observable<List<Photo>> fetchPhotos() {
        return AppNetworkManager.getAppClient().getPhotos()
                .compose(new AppRxSchedulers().applySchedulers()).retryWhen(RetryAPI.retryWithExponentialBackOff()).flatMap(list -> {
                    AsyncTask.execute(() -> {
                        updateApiEndTime(Constants.REQUEST_TYPE.PHOTOS);
                        updateDbSaveStartTime(Constants.REQUEST_TYPE.PHOTOS);
                        DbManager.getInstance().getDb().photosDao().insert(list);
                        updateDbSaveEndTime(Constants.REQUEST_TYPE.PHOTOS);
                    });
                    return Observable.just(list);
                });
    }

    @Override
    public Observable<List<ToDo>> fetchToDos() {
        return AppNetworkManager.getAppClient().getToDos()
                .compose(new AppRxSchedulers().applySchedulers()).retryWhen(RetryAPI.retryWithExponentialBackOff()).flatMap(list -> {
                    AsyncTask.execute(() -> {
                        updateApiEndTime(Constants.REQUEST_TYPE.TODOS);
                        updateDbSaveStartTime(Constants.REQUEST_TYPE.TODOS);
                        DbManager.getInstance().getDb().toDosDao().insert(list);
                        updateDbSaveEndTime(Constants.REQUEST_TYPE.TODOS);
                    });
                    return Observable.just(list);
                });
    }

    @Override
    public Observable<List<Post>> fetchPosts() {
        return AppNetworkManager.getAppClient().getPosts()
                .compose(new AppRxSchedulers().applySchedulers()).retryWhen(RetryAPI.retryWithExponentialBackOff()).flatMap(list -> {
                    AsyncTask.execute(() -> {
                        updateApiEndTime(Constants.REQUEST_TYPE.POSTS);
                        updateDbSaveStartTime(Constants.REQUEST_TYPE.POSTS);
                        DbManager.getInstance().getDb().postsDao().insert(list);
                        updateDbSaveEndTime(Constants.REQUEST_TYPE.POSTS);
                    });
                    return Observable.just(list);
                });
    }

    @Override
    public void updateApiStartTime(int type) {
        PingLog pingLog = getPingLog(type);
        pingLog.setApiStartTimestamp(new Date());
        insertPingLog(pingLog);
    }

    private void updateApiEndTime(int type) {
        PingLog pingLog = getPingLog(type);
        pingLog.setApiEndTimestamp(new Date());
        insertPingLog(pingLog);
    }

    private void insertPingLog(PingLog pingLog) {
        long insert = DbManager.getInstance().getDb().pingLogsDao().insert(pingLog);
        Log.i("ping_logs" , pingLog.toString() + " $$ is inserted : " + insert);
    }

    private void updateDbSaveStartTime(int type) {
        PingLog pingLog = getPingLog(type);
        pingLog.setSaveStartTimestamp(new Date());
        insertPingLog(pingLog);
    }

    private void updateDbSaveEndTime(int type) {
        PingLog pingLog = getPingLog(type);
        pingLog.setSaveEndTimestamp(new Date());
        insertPingLog(pingLog);
    }

    private PingLog getPingLog(int type) {
        PingLog pingLog = DbManager.getInstance().getDb().pingLogsDao().getPingLogById(type);
        if (pingLog == null) {
            pingLog = new PingLog(type);
        }
        return pingLog;
    }

    @Override
    public void clearPingLogs() {
        AsyncTask.execute(() -> {
            DbManager.getInstance().clearLogs();
        });
    }

    @Override
    public LiveData<List<PingLog>> observeAllData() {
        return DbManager.getInstance().getDb().pingLogsDao().observeAllData();
    }
}
