package com.morziz.pingcalculator.network.rx;

import io.reactivex.ObservableTransformer;
import io.reactivex.Scheduler;
import io.reactivex.SingleTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class AppRxSchedulers implements RxSchedulers {

    @Override
    public Scheduler androidUI() {
        return AndroidSchedulers.mainThread();
    }

    @Override
    public Scheduler io() {
        return Schedulers.io();
    }


    @Override
    public Scheduler computation() {
        return Schedulers.computation();
    }

    @Override
    public Scheduler immediate() {
        return Schedulers.trampoline();
    }

    public <T> ObservableTransformer<T, T> applySchedulers() {
        return observable -> observable.subscribeOn(io())
                .observeOn(androidUI());
    }

    public <T> SingleTransformer<T, T> applySingleSchedulers() {
        return observable -> observable.subscribeOn(io())
                .observeOn(androidUI());
    }
}