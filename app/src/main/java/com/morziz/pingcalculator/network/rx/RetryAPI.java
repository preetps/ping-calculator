package com.morziz.pingcalculator.network.rx;

import androidx.core.util.Pair;

import com.morziz.pingcalculator.network.utils.NetworkUtils;

import java.net.SocketTimeoutException;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.functions.Function;


public class RetryAPI {

    private static final int UNCHECKED_ERROR_TYPE_CODE = -100;// to pass the error

    public static Function<Observable<? extends Throwable>, Observable<?>> retryWithExponentialBackOff(){
        return exponentialBackoffForExceptions(TimeUnit.MICROSECONDS, SocketTimeoutException.class);
    }

    @SafeVarargs
    private static Function<Observable<? extends Throwable>, Observable<?>> exponentialBackoffForExceptions(final TimeUnit unit, final Class<? extends Throwable>... errorTypes) {
        long delays = NetworkUtils.getRetryDelay(); // get value from firebase
        int numOfRetries = (int) NetworkUtils.getNumOfRetries(); // get value from firebase
        final long initialDelay = delays < 0 ? NetworkUtils.DEFAULT_INITIAL_DELAY : delays;
        final int numRetries = numOfRetries < 0 ? NetworkUtils.DEFAULT_NUM_RETRIES : numOfRetries;
        //zipWith operator merges multiple observables
        return errors -> errors
                .zipWith(Observable.range(1, numRetries + 1), (error, integer) -> {
                    //here we add our termination error code
                    if (integer == numRetries + 1) {
                        return new Pair<>(error, UNCHECKED_ERROR_TYPE_CODE);
                    }

                    if (errorTypes != null) {
                        for (Class<? extends Throwable> clazz : errorTypes) {
                            if (clazz.isInstance(error)) {
                                // Mark as error type found
                                return new Pair<>(error, integer);
                            }
                        }
                    }

                    return new Pair<>(error, UNCHECKED_ERROR_TYPE_CODE);
                })
                .flatMap(errorRetryCountTuple -> { // iterate over each error

                    int retryAttempt = errorRetryCountTuple.second;

                    // If not a known error type, pass the error through.
                    if (retryAttempt == UNCHECKED_ERROR_TYPE_CODE) {
                        return Observable.error(errorRetryCountTuple.first);
                    }
                    //delay is calculated with power of initial delay by current attempt
                    long delay = (long) Math.pow(initialDelay, retryAttempt);

                    // Else, exponential backoff for the passed in error types.
                    return Observable.timer(delay, unit);
                });
    }
}
