package com.morziz.pingcalculator;

import android.app.Application;

import com.morziz.pingcalculator.db.DbManager;
import com.morziz.pingcalculator.network.manager.AppNetworkManager;

public class PingApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        AppNetworkManager.init(this);
        DbManager.getInstance().init(this);
    }
}
