package com.morziz.pingcalculator;

public interface Constants {

    int DELAY_IN_REQUEST = 5000;

    interface REQUEST_TYPE {
        int POSTS = 1;
        int COMMENTS = 2;
        int PHOTOS = 3;
        int TODOS = 4;

    }
}
