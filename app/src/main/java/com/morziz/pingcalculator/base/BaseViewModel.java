package com.morziz.pingcalculator.base;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * This is the base class for all view model classes.
 * Also clears all disposable when destroyed.
 */
public abstract class BaseViewModel extends ViewModel {

    //Holds reference of all disposable
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private final MutableLiveData<Boolean> isLoading;


    public BaseViewModel() {
        isLoading = new MutableLiveData<>();
    }

    public MutableLiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    public abstract void onLoad();

    public final void showLoader() {
        isLoading.setValue(true);

    }

    /**
     * Hides progress bar
     */
    public final void hideLoader() {
        isLoading.setValue(false);
    }


    private CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }

    protected void addRequestDisposable(Disposable disposable) {
        getCompositeDisposable().add(disposable);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        getCompositeDisposable().dispose();
    }
}
