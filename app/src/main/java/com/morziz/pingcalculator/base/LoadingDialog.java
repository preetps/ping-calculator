package com.morziz.pingcalculator.base;

import android.app.Activity;
import android.content.res.Resources;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;
import androidx.annotation.UiThread;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;

import com.morziz.pingcalculator.R;

import static androidx.annotation.RestrictTo.Scope.LIBRARY_GROUP;

/**
 * <h>Purpose:</h> This class is for showing processing ProgressDialog in Fraud Detection module.
 *
 * <p>
 * <h1>Scope:<h1/> Fraud Detection Module
 * <h1>Thread:<h1/> Always run on Main/UI Thread
 * <h1>Used In<h1/> :
 */
@MainThread
@RestrictTo(LIBRARY_GROUP)
public class LoadingDialog {
    /**
     * The logging tag used by this class with android.util.Log
     */
    private static final String TAG = LoadingDialog.class.getCanonicalName();
    /**
     * initial progress loading
     */
    public static final int PROGRESS_LOADING = 0;
    /**
     * ProgressDialog instance
     */
    private static AlertDialog progressDialog;
    private static AlertDialog.Builder builderAlertDialog;

    /**
     * Show Progress Dialog with provided type
     *
     * @param context caller context
     * @param type    progress type
     */

    /**
     * Default loading message
     */
    private static final String STR_DEFAULT_MSG = "Please wait...";

    @UiThread
    public static void showLoadingDialog(@NonNull Activity activity, int type) {

        showLoadingDialog(activity, type, null, 0);

    }

    /**
     * Show Progress Dialog with provided type and custom message
     *
     * @param activity caller context
     * @param type     progress type
     * @param message  custom title
     */
    public static void showLoadingDialog(@NonNull Activity activity, int type, String message) {

        showLoadingDialog(activity, type, message, 0);

    }

    /**
     * Show Progress Dialog with provided type and image
     *
     * @param activity   caller context
     * @param type       progress type
     * @param drawableId custom drawable id
     */
    public static void showLoadingDialog(@NonNull Activity activity, int type, int drawableId) {

        showLoadingDialog(activity, type, null, drawableId);

    }

    /**
     * Show Progress Dialog with provided type,custom message and image
     *
     * @param activity   caller context
     * @param type       progress type
     * @param message    custom title
     * @param drawableId custom drawable id
     */
    public static void showLoadingDialog(@NonNull Activity activity, int type, String message, int drawableId) {
        // check if provided context is not null
        if (null == activity) {
            return;
        }
        switch (type) {
            case PROGRESS_LOADING:
                showProgressLoading(activity, message);
                break;
            //@TODO will add more supported progress types if required

            default:
                showProgressLoading(activity, message);
                break;
        }
    }

    /**
     * Create and show Progress bar with provided message
     *
     * @param activity caller context
     * @param message  custom loading message
     */
    private static void showProgressLoading(@NonNull Activity activity, String message) {
        if (progressDialog == null) {

            ViewGroup viewGroup = activity.findViewById(android.R.id.content);
            //then we will inflate the custom alert dialog xml that we created
            View dialogView = LayoutInflater.from(activity).inflate(R.layout.view_progress_dialog, viewGroup, false);

            //Now we need an AlertDialog.Builder object
            builderAlertDialog = new AlertDialog.Builder(activity);

            //setting the view of the builder to our custom view that we already inflated
            builderAlertDialog.setView(dialogView);

            // if message is null set default message
            TextView dialogTitle = (TextView) dialogView.findViewById(R.id.progressbar_title);
            TextView dialogMsg = (TextView) dialogView.findViewById(R.id.progressbar_loading_msg);
            dialogTitle.setVisibility(View.GONE);

            dialogMsg.setText(null == message ? STR_DEFAULT_MSG : message);
            //finally creating the alert dialog and displaying it
            progressDialog = builderAlertDialog.create();
        }
        progressDialog.setCancelable(false);
        try {
            progressDialog.show();
            Resources resources = activity.getResources();
            int widthInPx = Math.round(TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP, 240, resources.getDisplayMetrics()));
            int heightInPx = Math.round(TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP, 100, resources.getDisplayMetrics()));
            progressDialog.getWindow().setLayout(widthInPx, heightInPx);

        } catch (Exception e) {
            Log.e(TAG, "progressDialog " + e);
        }
    }

    /**
     * Create and show Progress bar with provided message
     *
     * @param activity caller context
     * @param message  custom loading message
     */
    public static void showProgressLoading(@NonNull Activity activity,
                                           String strTitle,
                                           String message,
                                           boolean isCancelable) {
        if (progressDialog == null) {
            ViewGroup viewGroup = activity.findViewById(android.R.id.content);
            //then we will inflate the custom alert dialog xml that we created
            View dialogView = LayoutInflater.from(activity).inflate(R.layout.view_progress_dialog, viewGroup, false);

            //Now we need an AlertDialog.Builder object
            builderAlertDialog = new AlertDialog.Builder(activity);

            //setting the view of the builder to our custom view that we already inflated
            builderAlertDialog.setView(dialogView);
            //finally creating the alert dialog and displaying it
            progressDialog = builderAlertDialog.create();
            // if message is null set default message
            TextView dialogTitle = (TextView) dialogView.findViewById(R.id.progressbar_title);
            TextView dialogMsg = (TextView) dialogView.findViewById(R.id.progressbar_loading_msg);
            /*if (!TextUtils.isEmpty(strTitle)) {
                dialogTitle.setVisibility(View.VISIBLE);
                dialogTitle.setText(strTitle);
            } else {
                dialogTitle.setVisibility(View.GONE);
            }*/
            dialogTitle.setVisibility(View.GONE);
            dialogMsg.setText(null == message ? STR_DEFAULT_MSG : message);
        }

        progressDialog.setCancelable(isCancelable);
        progressDialog.setCanceledOnTouchOutside(isCancelable);
        progressDialog.setOnKeyListener((dialog, keyCode, event) -> keyCode == KeyEvent.KEYCODE_CAMERA || keyCode == KeyEvent.KEYCODE_SEARCH);
        try {
            progressDialog.show();
            Resources resources = activity.getResources();
            int widthInPx = Math.round(TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP, 240, resources.getDisplayMetrics()));
            /*int height130Px = Math.round(TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP, 130, resources.getDisplayMetrics()));*/
            int height100Px = Math.round(TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP, 100, resources.getDisplayMetrics()));
            /*if (!TextUtils.isEmpty(strTitle)) {
                progressDialog.getWindow().setLayout(widthInPx, height130Px);
            } else {

            }*/
            progressDialog.getWindow().setLayout(widthInPx, height100Px);
        } catch (Exception e) {
            Log.e(TAG, "progressDialog " + e);
        }
    }

    public static void setCancelable(boolean status) {
        if (progressDialog!=null){
            progressDialog.setCancelable(status);
            progressDialog.setCanceledOnTouchOutside(status);
        }

    }

    /**
     * Show progress bar with default message
     *
     * @param activity context from caller
     */
    public static void showLoadingDialog(@NonNull FragmentActivity activity) {
        showLoadingDialog(activity, PROGRESS_LOADING);
    }

    /**
     * cancel progress dialog
     */
    public static void cancelLoadingDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            try {
                progressDialog.dismiss();
            } catch (Exception e) {
                Log.e(TAG, "progressDialog " + e);
            }
        }
        progressDialog = null;
    }

    public static boolean isLoadingDialogShowing() {
        if (progressDialog != null && progressDialog.isShowing()) {
            return true;
        }
        return false;
    }

    public static String getDialogMessage() {
        String message = "";
        try {
            TextView textView = progressDialog.findViewById(R.id.progressbar_loading_msg);
            message = textView.getText().toString().trim();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }

    public static AlertDialog getProgressDialog() {
        return progressDialog;
    }

    public enum ORIENTATION {
        HORIZONTAL(0), VERTICAL(1);
        private int orientation;

        ORIENTATION(int value) {
            orientation = value;
        }

        int getValue() {
            return orientation;
        }
    }

}