package com.morziz.pingcalculator.base;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.morziz.pingcalculator.network.repomanager.PingRepoImpl;
import com.morziz.pingcalculator.pinging.PingViewModel;

public class ViewModelProviderFactory implements ViewModelProvider.Factory {

    private static ViewModelProviderFactory instance;

    private ViewModelProviderFactory() {
    }

    public static ViewModelProviderFactory getInstance() {
        if (instance == null) {
            instance = new ViewModelProviderFactory();
        }
        return instance;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(PingViewModel.class)) {
            return (T) new PingViewModel(PingRepoImpl.getInstance());
        }
        return null;
    }
}