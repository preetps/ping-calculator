package com.morziz.pingcalculator.base;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;


/**
 * Base class for all View Model Fragments. Responsible for data biding and view model binding
 * @param <T> , View Model class which extends BaseViewModel.class.
 */
public abstract class BaseMVVMFragment<V extends ViewDataBinding, T extends BaseViewModel> extends BaseFragment {

    protected V binding;
    protected T viewModel;

    public BaseMVVMFragment() {
        // Required empty public constructor
    }

    @Override
    protected final void initLayout(View view) {
        onViewModelCreated(view,viewModel);
        viewModel.onLoad();
    }


    /**
     * Call this method to show loading with msg.
     *//*
    protected void showCustomLoader() {
        getViewModel().getIsLoading().observe(this, aBoolean -> {
            if (aBoolean) {
                showProgressLoader(getString(R.string.submitting_details_), false, OaProgressDialog.ORIENTATION.HORIZONTAL);
            } else {
                dismissProgressLoader();
            }
        });

    }
*/

    @Override
    protected final View getContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater,
                getFragmentLayout(), container, false);
        viewModel = createViewModel();
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    public T getViewModel() {
        return viewModel;
    }

    public V getDataBinding(){
        return binding;
    }

    protected abstract T createViewModel();

    public abstract int getFragmentLayout();

    public abstract void onViewModelCreated(View view, T viewModel);

}
