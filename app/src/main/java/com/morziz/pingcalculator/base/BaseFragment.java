package com.morziz.pingcalculator.base;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.morziz.pingcalculator.R;

/**
 * Base for all the fragments, includes common functionality for basic features like fragment transactions, dialogs, toasts
 * <p>
 * #abstract class provides an option to fetch basic details like layout view and and onViewCreated callbacks for the the fragment
 * lifecycle support to the child fragment
 */
public abstract class BaseFragment extends DialogFragment {

    /**
     * abstract method to fetch data from child fragments to make default implementation in the parent fragment
     *
     * @param inflater           : {@link LayoutInflater} for inflating the view of the child fragment
     * @param container          : {@link ViewGroup} containing the fragment
     * @param savedInstanceState : {@link Bundle} for the savedState
     * @return {@link View} inflated using inflater
     */
    protected abstract View getContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);

    /**
     * Use this calllback in child fargment for UI related tasks, as it is called after View is created for the fragment
     *
     * @param view : inflated {@link View}
     */
    protected abstract void initLayout(View view);

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
        if (dialog != null) {
//               dialog.getWindow().getAttributes().windowAnimations = R.style.DialogSlideAnimation;
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            dialog.getWindow()
//                    .addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            }
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        return dialog;

    }


    @Override
    public final View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return getContentView(inflater, container, savedInstanceState);
    }

    @Override
    public final void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initLayout(view);
    }

    protected void onActivityForResult(int requestCode, int resultCode, Intent data) {
    }

    /**
     * Add fragment to the container provided
     *
     * @param fragmentContainer Container or layout id that will hold the fragment
     * @param fragment          {@link Fragment} object to be added
     * @param tag               String tag associated to identify the uniqueness of fragment
     */
    protected final void addFragment(int fragmentContainer, Fragment fragment, String tag) {
        if (isAdded()) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .add(fragmentContainer, fragment, tag)
                    .commit();
        }
    }

    /**
     * Add fragment to the container provided
     *
     * @param fragmentContainer Container or layout id that will hold the fragment
     * @param fragment          {@link Fragment} object to be added
     * @param tag               String tag associated to identify the uniqueness of fragment
     */
    protected final void addChildFragment(int fragmentContainer, Fragment fragment, String tag) {
        if (isAdded()) {
            getChildFragmentManager()
                    .beginTransaction()
                    .add(fragmentContainer, fragment, tag)
                    .commit();
        }
    }

    /**
     * Add fragment by not adding to the back stack
     *
     * @param fragmentContainer Container or layout id that will hold the fragment
     * @param fragment          {@link Fragment} object to be added
     */
    protected final void addFragmentToBackStack(int fragmentContainer, Fragment fragment) {
        if (isAdded()) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack(null)
                    .add(fragmentContainer, fragment)
                    .commit();
        }
    }

    /**
     * Add fragment by not adding to the back stack
     *
     * @param fragmentContainer Container or layout id that will hold the fragment
     * @param fragment          {@link Fragment} object to be added
     */
    protected final void addChildFragmentToBackStack(int fragmentContainer, Fragment fragment) {
        if (isAdded()) {
            getChildFragmentManager()
                    .beginTransaction()
                    .addToBackStack(null)
                    .add(fragmentContainer, fragment)
                    .commit();
        }
    }

    /**
     * Add fragment by not adding to the back stack
     *
     * @param fragment {@link Fragment} object to be added
     */
    protected final void showFragment(BaseFragment fragment) {
        if (isAdded()) {
            showFragment(fragment, getTag());
        }
    }

    /**
     * Add fragment by not adding to the back stack
     *
     * @param fragment {@link Fragment} object to be added
     */
    protected final void showFragment(BaseFragment fragment, String tag) {
        if (isAdded()) {
            fragment.show(getActivity().getSupportFragmentManager(), tag);
        }
    }

    /**
     * Replace fragment
     *
     * @param fragmentContainer Container or layout id that will hold the fragment
     * @param fragment          {@link Fragment} object to be added
     */
    protected final void replaceFragment(int fragmentContainer, Fragment fragment) {
        if (isAdded()) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack(null)
                    .replace(fragmentContainer, fragment, fragment.getClass().getSimpleName())
                    .commit();
        }
    }
    /**
     * Replace fragment
     *
     * @param fragmentContainer Container or layout id that will hold the fragment
     * @param fragment          {@link Fragment} object to be added
     */
    protected final void replaceFragmentFragmentManager(int fragmentContainer, Fragment fragment) {
        if (isAdded()) {
            getChildFragmentManager()
                    .beginTransaction()
                    .replace(fragmentContainer, fragment,fragment.getClass().getSimpleName())
                    .addToBackStack(fragment.getClass().getSimpleName())
                    .commit();
        }
    }

    /**
     * Replace fragment
     *
     * @param fragmentContainer Container or layout id that will hold the fragment
     * @param fragment          {@link Fragment} object to be added
     */
    protected final void replaceChildFragment(int fragmentContainer, Fragment fragment) {
        if (isAdded()) {
            getChildFragmentManager()
                    .beginTransaction()
                    .replace(fragmentContainer, fragment)
                    .commit();
        }
    }

    /**
     * Replace fragment without backstack
     *
     * @param fragmentContainer Container or layout id that will hold the fragment
     * @param fragment          {@link Fragment} object to be added
     */
    protected final void replaceFragmentWithoutBackStack(int fragmentContainer, Fragment fragment) {
        if (isAdded()) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(fragmentContainer, fragment)
                    .commitAllowingStateLoss();
        }
    }

    public final void popBackStack() {
        if (isAdded() && getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0) {

            getActivity().getSupportFragmentManager().popBackStackImmediate();
        }
    }

    public final void popBackStackChild() {
        if (isAdded() && getChildFragmentManager().getBackStackEntryCount() > 0) {
            getChildFragmentManager().popBackStackImmediate();
        }else {
            popBackStack();
        }
    }


    public final void popBackStackImmediate(FragmentManager manager) {
        if (isAdded() && manager.getBackStackEntryCount() > 0) {
            int count = getActivity().getSupportFragmentManager().getBackStackEntryCount();
            Fragment fragment = getActivity().getSupportFragmentManager()
                    .findFragmentByTag(manager.getBackStackEntryAt(count - 1).getName());
            if (fragment != null)
                getActivity().getSupportFragmentManager().beginTransaction().remove(fragment).commit();
        }
    }

    /* ------------  Dialogs, ProgressLoaders and Toast --------------- */

    /**
     * @param title              String title for the Dialog
     * @param message            String message for the Dialog
     * @param positiveButtonText String text for the positive button
     * @param negativeButtonText String text for the positive button
     * @param callback           {@link ActionCallback} callbacks for positive and negative button clicks
     */
    public final void showAlert(String title, String message, String positiveButtonText, String negativeButtonText, final ActionCallback callback) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity()).setTitle(title).setMessage(message)
                .setPositiveButton(positiveButtonText, (DialogInterface dialog, int which) -> {
                    dialog.dismiss();
                    if (callback != null) {
                        callback.onPositiveBtnClick();
                    }
                });
        if (negativeButtonText != null) {
            alertDialogBuilder.setNegativeButton(negativeButtonText, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (callback != null) {
                        callback.onNegativeBtnClick();
                    }
                }
            });
        }

        AlertDialog alertDialog = alertDialogBuilder.show();

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.dark));
        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.dark));
    }

    /**
     * Interface used to throw callback for alert dialogs positive and negative
     * buttons via {@link ActionCallback#onPositiveBtnClick()} and {@link ActionCallback#onNegativeBtnClick()} ()}
     */
    protected interface ActionCallback {
        void onPositiveBtnClick();

        void onNegativeBtnClick();

    }

    /**
     * Open Dialog for no internet connection where callbacks for its click can be obtained by {@link ActionCallback#onPositiveBtnClick()} callback
     *
     * @param actionCallback See {@link ActionCallback}
     */
    protected void openNoInternetDialog(ActionCallback actionCallback) {
        showAlert(getResources().getString(R.string.no_internet),
                getResources().getString(R.string.no_internet_msg),
                getResources().getString(R.string.ok), null, actionCallback);
    }

    /**
     * Open Technical error dialog with retry and receive callbacks
     * via {@link ActionCallback#onPositiveBtnClick()} for retry functionality and {@link ActionCallback#onNegativeBtnClick()} ()}
     * for cancel
     *
     * @param actionCallback See {@link ActionCallback}
     */
    protected void showTechnicalErrorRetryDialog(ActionCallback actionCallback) {
        showAlert(getResources().getString(R.string.technical_error_msg), true, actionCallback);
    }

    /**
     * Open Technical error dialog without retry
     */
    protected void showTechnicalErrorDialog() {
        showAlert(getResources().getString(R.string.technical_error_msg), false, null);
    }

    private void showAlert(String message, boolean isRetryEnabled, ActionCallback actionCallback) {
        if (isRetryEnabled) {
            showAlert(getResources().getString(R.string.technical_error), message, getResources().getString(R.string.retry), getResources().getString(R.string.cancel), actionCallback);
        } else {
            showAlert(getResources().getString(R.string.technical_error), message, getResources().getString(R.string.ok), null, actionCallback);
        }
    }

    /**
     * Show toast message
     *
     * @param message Custom message to display
     */
    public void showToast(String message) {
        if (isAdded()) {
            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
        }
    }


    /**
     * Show full screen progress loader with custom message and orientation
     *
     * @param message     Custom message to display
     * @param cancelable  : true to make progress loader cancellable else false
     * @param orientation See {@link LoadingDialog.ORIENTATION}. This refers to the orientation between text and loader that is it
     *                    {@link LoadingDialog.ORIENTATION#HORIZONTAL}
     *                    or  {@link LoadingDialog.ORIENTATION#VERTICAL}
     */
    protected void showProgressLoader(String message, boolean cancelable, LoadingDialog.ORIENTATION orientation) {
        if (isAdded()) {
            LoadingDialog.showProgressLoading(getActivity(),
                    null,
                    message,

                    cancelable);
           /* if (oaProgressDialog == null) {
                oaProgressDialog = new OaProgressDialog(getActivity());
            }
            if (!oaProgressDialog.isShowing()) {
                oaProgressDialog.setCancelable(cancelable);
                oaProgressDialog.setCanceledOnTouchOutside(cancelable);
                oaProgressDialog.show();
                oaProgressDialog.setText(message);
                oaProgressDialog.setDialogOrientation(orientation);
            }*/
        }
    }

    /**
     * Show full screen progress loader
     *
     * @param cancelable : true to make progress loader cancellable else false
     */
    protected final void showProgressLoader(boolean cancelable) {
        showProgressLoader(null, cancelable, LoadingDialog.ORIENTATION.HORIZONTAL);
    }

    /**
     * Dismiss Progress loader
     */
    protected void dismissProgressLoader() {
        LoadingDialog.cancelLoadingDialog();
        /*if (oaProgressDialog != null) {
            oaProgressDialog.dismiss();
        }*/
    }

    // Use this method only if the fragment is not replaced or added but shown using show() method of dialogfragment
    protected boolean isShowing(BaseFragment fragment) {
        return fragment != null && fragment.getDialog() != null && fragment.getDialog().isShowing();
    }


}
